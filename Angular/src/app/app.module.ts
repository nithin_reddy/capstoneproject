import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';


import { LoginComponent } from './admin/login/login.component';
import { SigninComponent } from './user/signin/signin.component';

import { QnAModuleComponent } from './qn-a-module/qn-a-module.component';
import { NewuserComponent } from './user/newuser/newuser.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
   
    AdminComponent,
    UserComponent,
    
   
    LoginComponent,
        SigninComponent,
        
        QnAModuleComponent,
                 NewuserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
