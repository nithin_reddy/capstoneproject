import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QnAModuleComponent } from './qn-a-module.component';

describe('QnAModuleComponent', () => {
  let component: QnAModuleComponent;
  let fixture: ComponentFixture<QnAModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QnAModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QnAModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
