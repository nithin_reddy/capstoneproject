package com.example.SpringPart.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
@Data
@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "user_name",nullable=false)
	private String user_name;
	
	@Column(name = "user_email",nullable=false)
	private String user_email;
	
	@Column(name = "password",nullable=false)
	private String password;
	
	
	
}
