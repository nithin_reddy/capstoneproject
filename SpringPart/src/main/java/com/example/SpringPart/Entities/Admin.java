package com.example.SpringPart.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "admin")
public class Admin {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "admin_name",nullable=false)
	private String admin_name;
	
	@Column(name = "admin_email",nullable=false)
	private String admin_email;
	
	@Column(name = "password",nullable=false)
	private String password;
	
	
	

}
