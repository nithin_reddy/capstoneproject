package com.example.SpringPart.Repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.SpringPart.Entities.Admin;


public interface AdminRepository extends JpaRepository<Admin, Integer>{

}
