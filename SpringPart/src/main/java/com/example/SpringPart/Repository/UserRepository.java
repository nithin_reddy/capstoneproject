package com.example.SpringPart.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.SpringPart.Entities.User;


public interface UserRepository extends JpaRepository<User, Integer> {

}
