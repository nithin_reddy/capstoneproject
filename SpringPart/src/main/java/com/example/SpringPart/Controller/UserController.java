package com.example.SpringPart.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.SpringPart.Entities.User;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	private UserController userService;
	
	public UserController(UserController userService) {
		super();
		this.userService = userService;
	}
	
	
	
		//build create user REST API
		@PostMapping
		public ResponseEntity<User> saveUser(@RequestBody User user){
			return new ResponseEntity<User>(HttpStatus.CREATED);
			
		}
			
		
		//build all user REST API
		@GetMapping
		public List<User> getUsers(){
			return ((Object) userService).getAllUser();
		}
		
		//build get user by id REST API
		//
		@GetMapping("{id}")
		public ResponseEntity<User> getUserById(@PathVariable("id") int userId){
			return new ResponseEntity<User>(HttpStatus.OK);
			

	
	
		}

}
