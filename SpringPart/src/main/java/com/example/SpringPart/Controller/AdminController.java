package com.example.SpringPart.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.SpringPart.Entities.Admin;
import com.example.SpringPart.service.AdminService;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
	
	private AdminService adminService;

	public AdminController(AdminService adminService) {
		super();
		this.adminService = adminService;
	}
	
	//build create admin REST API
	@PostMapping
	public ResponseEntity<Admin> saveAdmin(@RequestBody Admin admin){
		return new ResponseEntity<Admin>(adminService.saveAdmin(admin), HttpStatus.CREATED);
		
	}
	//build all all REST API
	@GetMapping
	public List<Admin> getAdmins(){
		return adminService.getAllAdmin();
	}
	
	//build get all by id REST API
	//
	@GetMapping("{id}")
	public ResponseEntity<Admin> getAdminById(@PathVariable("id") int adminId){
		return new ResponseEntity<Admin>(adminService.getAdminById(adminId), HttpStatus.OK);
		
	}
	}
