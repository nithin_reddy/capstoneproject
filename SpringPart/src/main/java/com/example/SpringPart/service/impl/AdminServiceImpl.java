package com.example.SpringPart.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.SpringPart.Entities.Admin;
import com.example.SpringPart.Repository.AdminRepository;
import com.example.SpringPart.exception.ResourceNotFoundException;
import com.example.SpringPart.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService{

	private AdminRepository adminRepository;
	 
	public AdminServiceImpl(AdminRepository adminRepository) {
		super();
		this.adminRepository = adminRepository;
	}

	@Override
	public Admin saveAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminRepository.save(admin);
	}

	@Override
	public List<Admin> getAllAdmin() {
		// TODO Auto-generated method stub
		return adminRepository.findAll();
	}

	@Override
	public Admin getAdminById(int id) {
		// TODO Auto-generated method stub
		Optional<Admin>  admin=adminRepository.findById(id);
		if(admin.isPresent()) {
			return admin.get();
		}else {
			throw new ResourceNotFoundException("Admin", "Id", id);
		}
	}

	@Override
	public Admin updateAdmin(Admin admin, int id) {
		// we need to check wether admin with given id is existing or not
		Admin existingAdmin = adminRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Admin", "Id", id));
	 
		
	
		
		return null;
	}
	

}
