package com.example.SpringPart.service;

import java.util.List;

import com.example.SpringPart.Entities.User;


public interface UserService {
	User saveUser(User user);
	List<User>getAllUser();
	User getUserById(int id);
	User updateUser(User user, int id);

}
