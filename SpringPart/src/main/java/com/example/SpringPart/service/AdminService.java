package com.example.SpringPart.service;

import java.util.List;

import com.example.SpringPart.Entities.Admin;

public interface AdminService {
	Admin saveAdmin(Admin admin);
	List<Admin>getAllAdmin();
	Admin getAdminById(int id);
	Admin updateAdmin(Admin admin, int id);
}
